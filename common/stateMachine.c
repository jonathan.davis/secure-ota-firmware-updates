/*
 * state_machine.c
 *
 *  Created on: Mar 22, 2020
 *      Author: Jonathan
 */

#include "stateMachine.h"

static const StateMachine_Struct StateMachine_defaultObject = {
        .currentState = NULL,
        .nextState = NULL,
        .transitionPending = false,
        .pendingEvents = 0
};

void StateMachine_construct(StateMachine_Struct *machine)
{
    *machine = StateMachine_defaultObject;

    Semaphore_Params params;
    Semaphore_Params_init(&params);
    params.mode = Semaphore_Mode_BINARY;
    Semaphore_construct(&machine->semaphore, 0, &params);
}

void StateMachine_setNextState(StateMachine_Struct *machine, StateMachine_State state)
{
    // Allow only one transition at a time
    if (!machine->transitionPending) {
        machine->transitionPending = true;
        machine->nextState = state;
        StateMachine_postEvents(machine, StateMachine_Event_Transition);
    }
}

StateMachine_EventMask StateMachine_pendEvents(StateMachine_Struct* machine, StateMachine_EventMask eventmask, uint32_t timeoutTicks)
{
    StateMachine_EventMask effectiveEvents = 0;

    // These events are always subscribed
    eventmask |= StateMachine_Event_Transition | StateMachine_Event_Timeout;

    while (1) {
        bool timeoutOccurred = !Semaphore_pend(Semaphore_handle(&machine->semaphore), timeoutTicks);

        if (timeoutOccurred) {
            machine->pendingEvents |= StateMachine_Event_Timeout;
        }

        effectiveEvents = machine->pendingEvents & eventmask;
        if (effectiveEvents != 0) {
            // Consider all returned events not pending anymore.
            machine->pendingEvents &= ~effectiveEvents;
            break;
        }
    }

    return effectiveEvents;
}

void StateMachine_postEvents(StateMachine_Struct* machine, StateMachine_EventMask eventmask)
{
    if (eventmask != 0) {
        machine->pendingEvents |= eventmask;

        Semaphore_post(Semaphore_handle(&machine->semaphore));
    }
}
