#ifndef COMM_H_
#define COMM_H_

#include <stdlib.h>
#include <xdc/std.h>

/* Drivers */
#include <ti/drivers/rf/RF.h>

#define RF_RAT_TICKS_PER_US     4

#define RF_convertUsToRatTicks(microseconds) \
    ((microseconds) * (RF_RAT_TICKS_PER_US))

#define RF_convertMsToRatTicks(milliseconds) \
    ((milliseconds) * 1000 * (RF_RAT_TICKS_PER_US))

#define RF_convertRatTicksToUs(ticks) \
    ((ticks) / (RF_RAT_TICKS_PER_US))

#define RF_convertRatTicksToMs(ticks) \
    ((ticks) / (1000 * (RF_RAT_TICKS_PER_US)))


void rfInit(uint8_t *packet, RF_Object *rfObject, RF_Handle *rfHandle, dataQueue_t *dataQueue, uint8_t maxPktLen);

void sendFirmware(RF_Handle * rfHandle, uint8_t version, uint8_t * packet, uint8_t *nonce);

void setHeaderPacket(uint8_t * packet, uint8_t numPackets, uint8_t numPages, uint8_t *nonce);

void setHeader(uint8_t * packet, uint8_t pageNum, uint8_t version, uint8_t packetNum, uint8_t packetLength);

uint8_t transmitPacket(RF_Handle * rfHandle);

void encrypt(uint8_t *plaintext, uint32_t plaintextLength, uint8_t *header, uint8_t headerLength, uint8_t *nonce);

void decrypt(uint8_t *cyphertext, uint32_t plaintextLength, uint8_t *header, uint8_t headerLength, uint8_t *nonce);

#endif /* COMM_H_ */
