/*
 * Copyright (c) 2017, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


#ifndef STATEMACHINE_H_
#define STATEMACHINE_H_


#include <ti/sysbios/knl/Semaphore.h>

#include <stdbool.h>
#include <stdint.h>

typedef void (*StateMachine_StateHandlerFunction)(void);
typedef StateMachine_StateHandlerFunction StateMachine_State;
typedef uint32_t StateMachine_EventMask;

typedef enum {
    StateMachine_Event00 = 1 << 0,
    StateMachine_Event01 = 1 << 1,
    StateMachine_Event02 = 1 << 2,
    StateMachine_Event03 = 1 << 3,
    StateMachine_Event04 = 1 << 4,
    StateMachine_Event05 = 1 << 5,
    StateMachine_Event06 = 1 << 6,
    StateMachine_Event07 = 1 << 7,
    StateMachine_Event08 = 1 << 8,
    StateMachine_Event09 = 1 << 9,
    StateMachine_Event10 = 1 << 10,
    StateMachine_Event11 = 1 << 11,
    StateMachine_Event12 = 1 << 12,
    StateMachine_Event13 = 1 << 13,
    StateMachine_Event14 = 1 << 14,
    StateMachine_Event15 = 1 << 15,
    StateMachine_Event16 = 1 << 16,
    StateMachine_Event17 = 1 << 17,
    StateMachine_Event18 = 1 << 18,
    StateMachine_Event19 = 1 << 19,
    StateMachine_Event20 = 1 << 20,
    StateMachine_Event21 = 1 << 21,
    StateMachine_Event22 = 1 << 22,
    StateMachine_Event23 = 1 << 23,
    StateMachine_Event24 = 1 << 24,
    StateMachine_Event25 = 1 << 25,
    StateMachine_Event26 = 1 << 26,
    StateMachine_Event27 = 1 << 27,
    StateMachine_Event28 = 1 << 28,
    StateMachine_Event29 = 1 << 29,
    StateMachine_Event_Timeout = 1 << 30,
    StateMachine_Event_Transition = (uint32_t)(1 << 31),
} StateMachine_Event;

#define STATE_MACHINE_EVENTS_ALL    0xFFFFFFFF

#define StateMachine_DECLARE_STATE(state) \
    void state##Function(); \
    static const StateMachine_State state = &state##Function;

typedef struct {
    volatile StateMachine_State currentState;
    volatile StateMachine_State nextState;
    volatile bool transitionPending;

    volatile uint32_t pendingEvents;

    Semaphore_Struct semaphore;
} StateMachine_Struct;

void StateMachine_construct(StateMachine_Struct *object);

void StateMachine_exec(StateMachine_Struct *machine, StateMachine_State initialState);

void StateMachine_setNextState(StateMachine_Struct *machine, StateMachine_State state);

StateMachine_EventMask StateMachine_pendEvents(StateMachine_Struct* machine, StateMachine_EventMask eventmask, uint32_t timeoutTicks);

void StateMachine_postEvents(StateMachine_Struct* machine, StateMachine_EventMask eventmask);


#endif /* STATEMACHINE_H_ */
