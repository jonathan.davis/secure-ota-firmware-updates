#include <flash.h>
#include <stdlib.h>

#include <xdc/std.h>
#include <xdc/runtime/System.h>

/* Drivers */
#include <ti/drivers/rf/RF.h>
#include <stdio.h>

/* Board Header files */
#include "Board.h"
#include "smartrf_settings/smartrf_settings.h"
#include <ti/sysbios/knl/Task.h>
#include "comm.h"
#include "RFQueue.h"
#include "math.h"
#include <ti/drivers/crypto/CryptoCC26XX.h>

#define FLASH_PAGE_SIZE        0x000001000
#define FW1_START_PAGE  10

void rfInit(uint8_t *packet, RF_Object *rfObject, RF_Handle *rfHandle, dataQueue_t *dataQueue, uint8_t maxPktLen)
{
    // TX cmd setup
    RF_cmdPropTx.pktLen = 0;
    RF_cmdPropTx.pPkt = packet;
    RF_cmdPropTx.startTrigger.triggerType = TRIG_ABSTIME;
    RF_cmdPropTx.startTime = 0;
    RF_cmdPropTx.pktConf.bUseCrc = 0x1;

    // RX cmd setup
    RF_cmdPropRx.startTrigger.triggerType = TRIG_NOW;
    RF_cmdPropRx.pQueue = dataQueue;           /* Set the Data Entity queue for received data */
    RF_cmdPropRx.rxConf.bAutoFlushIgnored = 1;  /* Discard ignored packets from Rx queue */
    RF_cmdPropRx.rxConf.bAutoFlushCrcErr = 1;   /* Discard packets with CRC error from Rx queue */
    RF_cmdPropRx.rxConf.bIncludeHdr = 1;
    RF_cmdPropRx.rxConf.bAppendTimestamp = 1;
    RF_cmdPropRx.maxPktLen = maxPktLen;        /* Implement packet length filtering to avoid PROP_ERROR_RXBUF */
    RF_cmdPropRx.pktConf.bRepeatOk = 1;
    RF_cmdPropRx.pktConf.bRepeatNok = 1;

    RF_Params rfParams;
    RF_Params_init(&rfParams);

    *rfHandle = RF_open(rfObject, &RF_prop, (RF_RadioSetup*)&RF_cmdPropRadioDivSetup, &rfParams);
    if (rfHandle == NULL) {
        System_abort("Failed to open RF module\n");
    }

    /* Set the frequency */
    RF_postCmd(*rfHandle, (RF_Op*)&RF_cmdFs, RF_PriorityNormal, NULL, 0);
}

void sendFirmware(RF_Handle * rfHandle, uint8_t version, uint8_t * packet, uint8_t *nonce) {
    //send firmware
    uint32_t readLoc = FLASH_PAGE_SIZE * FW1_START_PAGE;
    uint8_t numPages = 30 - FW1_START_PAGE;
    //generate random integers for nonce
    int k;
    for(k = 2; k < 12; k++) {
        nonce[k] = (uint8_t)((sqrt(RF_getCurrentTime()) - ((int)sqrt(RF_getCurrentTime()))) * (100000000));
    }
    uint8_t i;
    for(i = 0; i < numPages; i++) {
    //send page
        uint8_t numPackets = 37;
        setHeader(packet, i, version, 0, 26);
        setHeaderPacket(packet, numPackets, numPages, nonce);
        nonce[0] = 0;
        nonce[1] = i;
        Task_sleep(500);
        transmitPacket(rfHandle);
        uint8_t j;
        for(j = 0; j < numPackets; j++) {
            uint8_t dataLength = 112;
            if(j == 36) {
                dataLength = 64;
            }
            setHeader(packet, i, version, j + 1, dataLength + 12);
            //read data from flash
            readPage(&(packet[8]), readLoc, dataLength);
            //calculate nonce
            //first 3 bytes page/packet/transmission number
            //next 12 based on rand integers
            nonce[0] = i; //page num
            nonce[1] = j + 1; //packet num

            //encrypt packet
            encrypt(packet + 8, dataLength, packet, 8, nonce);
            transmitPacket(rfHandle);
            Task_sleep(100);
            readLoc = readLoc + dataLength;
        }
    }
}

void setHeaderPacket(uint8_t * packet, uint8_t numPackets, uint8_t numPages, uint8_t* nonce) {
    //num packets
    packet[8] = numPackets;
    packet[9] = numPages;

    //initial vector
    int i;
    for(i = 2; i < 12; i++) {
        packet[10 + i] = nonce[i];
    }
}

void setHeader(uint8_t * packet, uint8_t pageNum, uint8_t version, uint8_t packetNum, uint8_t packetLength) {
    RF_cmdPropTx.pktLen = packetLength;

    packet[0] = 34;
    packet[1] = 152;

    //PANNum
    packet[2] = 0;
    packet[3] = 0;

    packet[4] = version;
    packet[5] = packetLength;
    packet[6] = packetNum;
    packet[7] = pageNum;
}


uint8_t transmitPacket(RF_Handle * rfHandle) {
    //counter++;

    /* Send packet */
    RF_EventMask result = RF_runCmd(*rfHandle, (RF_Op*)&RF_cmdPropTx, RF_PriorityNormal, NULL, 0);
    if (!(result & RF_EventLastCmdDone))
    {
        return 1;
    }
    return 0;
}

void encrypt(uint8_t *plaintext, uint32_t plaintextLength, uint8_t *header, uint8_t headerLength, uint8_t *nonce) {

    uint8_t macLength = 4;

    // Holds the AES-CCM setup for this example
    uint8_t key[16] = {104, 139, 120, 63, 57, 255, 61, 29, 40, 120, 241, 68, 18, 50, 97, 113};

    int i;
    for(i = 0; i < 12; i++) {
        //nonce[i] = secure random number
    }
    CryptoCC26XX_Handle             handle;
    int32_t                         keyIndex;
    CryptoCC26XX_AESCCM_Transaction trans;
    int32_t                         status;

    // Initialize Crypto driver structures
    CryptoCC26XX_init();

    // Open the crypto hardware with non-exclusive access and default parameters.
    handle = CryptoCC26XX_open(Board_CRYPTO, false, NULL);
    if (handle == NULL) {
        System_abort("CryptoCC26XX did not open");
    }

    // Allocate a key storage location in the hardware
    keyIndex = CryptoCC26XX_allocateKey(handle, CRYPTOCC26XX_KEY_0, (const uint32_t *) key);
    if (keyIndex == CRYPTOCC26XX_STATUS_ERROR) {
        System_abort("Key Location was not allocated.");
    }

    // Encrypt and authenticate the message
    CryptoCC26XX_Transac_init((CryptoCC26XX_Transaction *) &trans, CRYPTOCC26XX_OP_AES_CCM);
    trans.keyIndex   = keyIndex;
    trans.authLength = macLength;
    trans.nonce  = (char *) nonce;
    trans.header = (char *) header;
    trans.fieldLength  = 3;
    trans.msgInLength  = plaintextLength;
    trans.headerLength = headerLength;
    trans.msgIn  = (char *) &(plaintext[0]);                // Message is encrypted in place
    trans.msgOut = (char *) &(plaintext[plaintextLength]);  // MAC will be written to this position
    status = CryptoCC26XX_transact(handle, (CryptoCC26XX_Transaction *) &trans);
    if (status != CRYPTOCC26XX_STATUS_SUCCESS) {
        System_abort("Encryption and signing failed.");
    }



    // Release the key location
    status = CryptoCC26XX_releaseKey(handle, &keyIndex);
    if (status != CRYPTOCC26XX_STATUS_SUCCESS) {
        System_abort("Key release was not successful.");
    }

    CryptoCC26XX_close(handle);

}

void decrypt(uint8_t *cyphertext, uint32_t plaintextLength, uint8_t *header, uint8_t headerLength, uint8_t *nonce) {
    uint8_t macLength = 4;
    CryptoCC26XX_Handle             handle;
    int32_t                         keyIndex;
    CryptoCC26XX_AESCCM_Transaction trans;
    int32_t                         status;

    // Holds the AES-CCM setup for this example
    uint8_t key[16] = { 104, 139, 120, 63, 57, 255, 61, 29, 40, 120, 241, 68, 18, 50, 97, 113};

    // Initialize Crypto driver structures
    CryptoCC26XX_init();

    // Open the crypto hardware with non-exclusive access and default parameters.
    handle = CryptoCC26XX_open(Board_CRYPTO, false, NULL);
    if (handle == NULL) {
        System_abort("CryptoCC26XX did not open");
    }

    // Allocate a key storage location in the hardware
    keyIndex = CryptoCC26XX_allocateKey(handle, CRYPTOCC26XX_KEY_0, (const uint32_t *) key);
    if (keyIndex == CRYPTOCC26XX_STATUS_ERROR) {
        System_abort("Key Location was not allocated.");
    }

    // Decrypt and authenticate message
    CryptoCC26XX_Transac_init((CryptoCC26XX_Transaction *) &trans, CRYPTOCC26XX_OP_AES_CCMINV);
    trans.keyIndex   = keyIndex;
    trans.authLength = macLength;
    trans.nonce  = (char *) nonce;
    trans.header = (char *) header;
    trans.fieldLength  = 3;
    trans.msgInLength  = macLength + plaintextLength;
    trans.headerLength = headerLength;
    trans.msgIn  = (char *) &(cyphertext[0]);                // Message is decrypted in place
    trans.msgOut = (char *) &(cyphertext[plaintextLength]);  // Points to the MAC, is used as input here

    // Do AES-CCM decryption and authentication
    status = CryptoCC26XX_transact(handle, (CryptoCC26XX_Transaction *) &trans);
    if(status != CRYPTOCC26XX_STATUS_SUCCESS){
        System_abort("Decryption and authentication failed.");
    }

    // Release the key location
    status = CryptoCC26XX_releaseKey(handle, &keyIndex);
    if (status == CRYPTOCC26XX_STATUS_ERROR) {
        //asdf
    }
    if (status != CRYPTOCC26XX_STATUS_SUCCESS) {
        System_abort("Key release was not successful.");
    }

    CryptoCC26XX_close(handle);

}
