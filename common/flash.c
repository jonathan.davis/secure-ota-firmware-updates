/* Standard C Libraries */
#include <stdlib.h>

/* DriverLib Header Files */
#include <driverlib/flash.h>

/* Board Header files */
#include "Board.h"

/* XDC Module Headers */
#include <xdc/std.h>
#include <xdc/runtime/System.h>

#include "flash.h"

#define FLASH_PAGE_SIZE     0x00001000

void erasePage(int pageNumber) {
    int j = FlashSectorErase(pageNumber*FLASH_PAGE_SIZE);
    if(j == 0x00000000) {
        //System_printf("FAPI_STATUS_SUCCESS\n");
        //System_flush();
        //System_printf("Page Erased: %i\n\n", pageNumber);
        //System_flush();
        return;
    }
    else if(j == 0x00000003) {
        //System_printf("FAPI_STATUS_INCORRECT_DATABUFFER_LENGTH\n");
        //System_flush();
        erasePage(pageNumber);
        return;
    }
    else if(j == 0x00000004) {
        //System_printf("FAPI_STATUS_FSM_ERROR\n");
        //System_flush();
        erasePage(pageNumber);
        return;
    }
    else {
        //System_printf("OTHER\n");
        //System_flush();
        erasePage(pageNumber);
        return;
    }
}

void writePage(uint8_t *writeDataBuffer, int startAddress, int writeBufferSize) {
    int j = FlashProgram(writeDataBuffer, startAddress, writeBufferSize);
    if(j == 0x00000000) {
        //System_printf("FAPI_STATUS_SUCCESS\n");
        //System_flush();
        //System_printf("Start Address Written To: %x\n\n", startAddress);
        //System_flush();
        return;
    }
    else if(j == 0x00000003) {
        //System_printf("FAPI_STATUS_INCORRECT_DATABUFFER_LENGTH\n");
        //System_flush();
        writePage(writeDataBuffer, startAddress, writeBufferSize);
        return;
    }
    else if(j == 0x00000004) {
        //System_printf("FAPI_STATUS_FSM_ERROR\n");
        //System_flush();
        writePage(writeDataBuffer, startAddress, writeBufferSize);
        return;
    }
    else {
        //System_printf("OTHER\n");
        //System_flush();
        writePage(writeDataBuffer, startAddress, writeBufferSize);
        return;
    }
}

void readPage(uint8_t *readDataBuffer, int startAddress, int readBufferSize) {
    memcpy(readDataBuffer, startAddress, readBufferSize);

    //System_printf("The Following was Read: ");
    //uint8_t i;
    //for(i=0; i<readBufferSize; i++){
    //    System_printf("%i ", readDataBuffer[i]);
    //    System_flush();
    //}
    //System_printf("\n\n");
    //System_flush();
}
