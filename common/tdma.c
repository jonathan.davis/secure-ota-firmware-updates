/*
 * tdma.c
 *
 *  Created on: Mar 18, 2020
 *      Author: Jonathan
 */

#include <tdma.h>

#include "smartrf_settings/smartrf_settings.h"


void setTDMAPacket(uint8_t *payload, union TDMAPacket *packet)
{
    switch (packet->header.packetType) {
    case TDMA_PACKET_TYPE_INFO:
        memcpy(payload, packet, sizeof(struct TDMAInfo));
        break;
    case TDMA_PACKET_TYPE_REQUEST:
        memcpy(payload, packet, sizeof(struct TDMARequest));
        break;
    case TDMA_PACKET_TYPE_ASSIGN:
        memcpy(payload, packet, sizeof(struct TDMAAssign));
        break;
    case TDMA_PACKET_TYPE_DATA:
        memcpy(payload, packet, sizeof(struct TDMAData));
        break;
    default:
        // Invalid packet
        break;
    }
}


uint8_t sendPacket(RF_Handle *rfHandle)
{
    /* Send packet */
    RF_EventMask result = RF_runCmd(*rfHandle, (RF_Op*)&RF_cmdPropTx, RF_PriorityNormal, NULL, 0);

    if (!(result & RF_EventLastCmdDone)) {
        return 1;
    }

    return 0;
}
