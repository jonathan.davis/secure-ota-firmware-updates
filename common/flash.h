#ifndef FLASH_H_
#define FLASH_H_

#include <stdint.h>

void erasePage(int pageNumber);

void writePage(uint8_t *writeDataBuffer, int startAddress, int writeBufferSize);

void readPage(uint8_t *readDataBuffer, int startAddress, int readBufferSize);

#endif /* FLASH_H_ */
