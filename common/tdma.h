/*
 * tdma.h
 *
 *  Created on: Mar 18, 2020
 *      Author: Jonathan
 */

#ifndef COMMON_TDMA_H_
#define COMMON_TDMA_H_


#include <stdint.h>

#include <ti/drivers/rf/RF.h>

/* Protocol information */
#define N_SLOTS     4       // Slots per frame
#define N_FRAMES    4       // Frames per cycle (must be >= number of nodes)
#define SLOT_LEN    50      // Transmit time in ms

/* Packet types */
#define TDMA_PACKET_TYPE_INFO       0
#define TDMA_PACKET_TYPE_REQUEST    1
#define TDMA_PACKET_TYPE_ASSIGN     2
#define TDMA_PACKET_TYPE_DATA       3

/* Reserved slots */
#define TDMA_SLOT_BEACON            0x00
#define TDMA_SLOT_REQUEST           0x01

/* Reserved addresses */
#define TDMA_ADDRESS_COORDINATOR    0x00
#define TDMA_ADDRESS_BROADCAST      0xFF

struct TDMAHeader {
    uint8_t destinationAddress;
    uint8_t sourceAddress;
    uint8_t packetType;
};

struct TDMAInfo {
    struct TDMAHeader header;
    uint32_t timestamp; // Timestamp of when packet sent (RAT ticks)
    uint32_t nextTime;  // Timestamp of next slot (RAT ticks)
    uint8_t nSlots;     // Number of slots in frame
    uint16_t slotLen;   // Length of slots in ms
    uint8_t nFree;      // Number of free slots available
};

struct TDMARequest {
    struct TDMAHeader header;
    uint32_t nonce;
};

struct TDMAAssign {
    struct TDMAHeader header;
    uint32_t nonce;
    uint8_t slot;
};

struct TDMAData {
    struct TDMAHeader header;
    uint16_t size;
};

union TDMAPacket {
    struct TDMAHeader header;
    struct TDMAInfo infoPacket;
    struct TDMARequest requestPacket;
    struct TDMAAssign assignPacket;
    struct TDMAData dataPacket;
};

void setTDMAPacket(uint8_t *payload, union TDMAPacket *packet);

uint8_t sendPacket(RF_Handle *rfHandle);


#endif /* COMMON_TDMA_H_ */
