#include <stdlib.h>
#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/knl/Task.h>
#include <xdc/std.h>
#include <xdc/runtime/System.h>

#include <Board.h>

#include <coordinatorTask.h>


/***** Defines *****/
#define COORDINATOR_TASK_STACK_SIZE  1024
#define COORDINATOR_TASK_PRIORITY    2

Task_Struct coordinatorTask;
static uint8_t coordinatorTaskStack[COORDINATOR_TASK_STACK_SIZE];


/*
 *  ======== main ========
 */
int main(void)
{
    /* Call board init functions. */
    Board_initGeneral();

    coordinatorTaskInit();

    Task_Params params;
    Task_Params_init(&params);
    params.stackSize = COORDINATOR_TASK_STACK_SIZE;
    params.priority = COORDINATOR_TASK_PRIORITY;
    params.stack = &coordinatorTaskStack;
    Task_construct(&coordinatorTask, (Task_FuncPtr)&coordinatorTaskFunction, &params, NULL);

    /* Start BIOS */
    BIOS_start();

    return (0);
}
