/*
 * coordinatorTask.h
 *
 *  Created on: Mar 22, 2020
 *      Author: Jonathan
 */

#ifndef COORDINATORTASK_H_
#define COORDINATORTASK_H_


void coordinatorTaskInit();
void coordinatorTaskFunction(UArg arg0, UArg arg1);


#endif /* COORDINATORTASK_H_ */
