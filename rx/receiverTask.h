/*
 * receiverTask.h
 *
 *  Created on: Mar 22, 2020
 *      Author: Jonathan
 */

#ifndef RECEIVERTASK_H_
#define RECEIVERTASK_H_


void receiverTaskInit();
void receiverTaskFunction(UArg arg0, UArg arg1);


#endif /* RECEIVERTASK_H_ */
