#include <stdlib.h>
#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/knl/Task.h>
#include <xdc/std.h>
#include <xdc/runtime/System.h>

#include <Board.h>

#include <receiverTask.h>


/***** Defines *****/
#define RECEIVER_TASK_STACK_SIZE  1024
#define RECEIVER_TASK_PRIORITY    2

Task_Struct receiverTask;
static uint8_t receiverTaskStack[RECEIVER_TASK_STACK_SIZE];


/*
 *  ======== main ========
 */
int main(void)
{
    /* Call board init functions. */
    Board_initGeneral();

    receiverTaskInit();

    Task_Params params;
    Task_Params_init(&params);
    params.stackSize = RECEIVER_TASK_STACK_SIZE;
    params.priority = RECEIVER_TASK_PRIORITY;
    params.stack = &receiverTaskStack;
    Task_construct(&receiverTask, (Task_FuncPtr)&receiverTaskFunction, &params, NULL);

    /* Start BIOS */
    BIOS_start();

    return (0);
}
