/*
 * receiverTask.c
 *
 *  Created on: Mar 22, 2020
 *      Author: Jonathan
 */

/***** Includes *****/
#include <stdlib.h>
#include <ti/sysbios/knl/Semaphore.h>
#include <ti/sysbios/knl/Task.h>
#include <xdc/std.h>
#include <xdc/runtime/System.h>

/* Drivers */
#include <ti/drivers/PIN.h>
#include <ti/drivers/rf/RF.h>

/* Board Header files */
#include <Board.h>

/* Application Header files */
#include <comm.h>
#include <receiverTask.h>
#include <RFQueue.h>
#include <smartrf_settings/smartrf_settings.h>
#include <stateMachine.h>
#include <tdma.h>

/* Pin driver handle */
static PIN_Handle ledPinHandle;
static PIN_State ledPinState;

/*
 * Application LED pin configuration table:
 *   - All LEDs board LEDs are off.
 */
PIN_Config pinTable[] = {
    Board_LED0 | PIN_GPIO_OUTPUT_EN | PIN_GPIO_LOW | PIN_PUSHPULL | PIN_DRVSTR_MAX,
    Board_LED1 | PIN_GPIO_OUTPUT_EN | PIN_GPIO_LOW | PIN_PUSHPULL | PIN_DRVSTR_MAX,
    PIN_TERMINATE
};


/***** Defines *****/
#define PAYLOAD_LENGTH      128
#define NUM_DATA_ENTRIES       2    /* NOTE: Only two data entries supported at the moment */
#define NUM_APPENDED_BYTES     5    // 1 status byte, 4 rx timestamp bytes appended
#define FLASH_PAGE_SIZE     0x00001000

#define SYMBOL_RATE            50000                         /* 50 kbits per second */
#define US_PER_SYMBOL          (1000000 / SYMBOL_RATE)
#define PREAMBLE_BITS          32
#define SYNCWORD_BITS          32

#define RX_START_MARGIN        RF_convertUsToRatTicks(500)   /* An arbitrarily chosen value to compensate for
                                                              * the potential drift of the RAT and the RTC. */

#define RX_TIMEOUT_TICKS       RF_convertUsToRatTicks((PREAMBLE_BITS + SYNCWORD_BITS) * US_PER_SYMBOL)
                                                             /* Tight, but ideal duration for receiving all bits of
                                                              * the preamble and the sync word. */

#define RX_TIMEOUT_MARGIN      RF_convertUsToRatTicks(1000)  /* Arbitrarily chosen margin added to the RX timeout
                                                              * to compensate calculation errors. */

#define RX_START_TO_SETTLE_TICKS   256 /* Time between RX start trigger and the radio
                                        * being ready to receive the first preamble bit.
                                        * This is a fixed value for CMD_PROP_RX. */
#define TX_START_TO_PREAMBLE_TICKS 384 /* Time between TX start trigger and first bit on air.
                                        * This is a fixed value for CMD_PROP_TX. */


typedef enum {
    Event_SlotChanged = StateMachine_Event00,
    Event_PacketReceived = StateMachine_Event01,
    Event_InvalidPacketReceived = StateMachine_Event02
} Event;

/***** Prototypes *****/
StateMachine_DECLARE_STATE(tdmaSyncState);
StateMachine_DECLARE_STATE(tdmaRequestState);
StateMachine_DECLARE_STATE(tdmaReceiveState);
StateMachine_DECLARE_STATE(tdmaTransmitState);

void advanceTDMASlot(UArg arg0);
void rfRxCallback(RF_Handle h, RF_CmdHandle ch, RF_EventMask e);

/***** Variable declarations *****/
Clock_Struct clkStruct;
Clock_Handle clkHandle;

Semaphore_Handle slotSemaphore;

RF_Object rfObject;
RF_Handle rfHandle;
RF_CmdHandle rfCmdHandle;

/* Buffer which contains all Data Entries for receiving data.
 * Pragmas are needed to make sure this buffer is 4 byte aligned (requirement from the RF Core) */
#if defined(__TI_COMPILER_VERSION__)
    #pragma DATA_ALIGN (rxDataEntryBuffer, 4);
        static uint8_t rxDataEntryBuffer[RF_QUEUE_DATA_ENTRY_BUFFER_SIZE(NUM_DATA_ENTRIES,
                                                                 PAYLOAD_LENGTH,
                                                                 NUM_APPENDED_BYTES)];
#elif defined(__IAR_SYSTEMS_ICC__)
    #pragma data_alignment = 4
        static uint8_t rxDataEntryBuffer[RF_QUEUE_DATA_ENTRY_BUFFER_SIZE(NUM_DATA_ENTRIES,
                                                                 PAYLOAD_LENGTH,
                                                                 NUM_APPENDED_BYTES)];
#elif defined(__GNUC__)
        static uint8_t rxDataEntryBuffer [RF_QUEUE_DATA_ENTRY_BUFFER_SIZE(NUM_DATA_ENTRIES,
            PAYLOAD_LENGTH, NUM_APPENDED_BYTES)] __attribute__ ((aligned (4)));
#else
    #error This compiler is not supported.
#endif

static dataQueue_t dataQueue;


StateMachine_Struct stateMachine;

static uint32_t timeOffset = 0;
static uint8_t currentSlot = 0;
static uint8_t assignedSlot = TDMA_SLOT_REQUEST;
static uint8_t txPacket[PAYLOAD_LENGTH + NUM_APPENDED_BYTES];
static uint8_t rxPacket[PAYLOAD_LENGTH + NUM_APPENDED_BYTES];
static uint32_t rxTime = 0;


void receiverTaskInit()
{
    /* Open LED pins */
    ledPinHandle = PIN_open(&ledPinState, pinTable);
    if(!ledPinHandle) {
        System_abort("Error initializing board LED pins\n");
    }

    // Initialize slot change semaphore
    Semaphore_Params semParams;
    Semaphore_Params_init(&semParams);
    semParams.mode = Semaphore_Mode_BINARY;
    slotSemaphore = Semaphore_create(1, &semParams, NULL);

    // Initialize slotting clock
    Clock_Params clkParams;
    Clock_Params_init(&clkParams);
    clkParams.period = 0;
    clkParams.startFlag = FALSE;
    Clock_construct(&clkStruct, (Clock_FuncPtr)advanceTDMASlot, (SLOT_LEN*1000)/Clock_tickPeriod, &clkParams);
    clkHandle = Clock_handle(&clkStruct);

    // Initialize RF module
    if(RFQueue_defineQueue(&dataQueue, rxDataEntryBuffer, sizeof(rxDataEntryBuffer), NUM_DATA_ENTRIES, PAYLOAD_LENGTH + NUM_APPENDED_BYTES)) {
        System_abort("Failed to allocate space for all data entries\n");
    }
    rfInit(txPacket, &rfObject, &rfHandle, &dataQueue, PAYLOAD_LENGTH);

    // Setup state machine
    StateMachine_construct(&stateMachine);
    stateMachine.nextState = tdmaSyncState;
    stateMachine.currentState = tdmaSyncState;
    stateMachine.transitionPending = false;
}

void advanceTDMASlot(UArg arg0)
{
    currentSlot = (currentSlot+1) % N_SLOTS;

    if (currentSlot == TDMA_SLOT_BEACON) {
        StateMachine_setNextState(&stateMachine, tdmaSyncState);
    } else if (assignedSlot == TDMA_SLOT_REQUEST && currentSlot == TDMA_SLOT_REQUEST) {
        StateMachine_setNextState(&stateMachine, tdmaRequestState);
    } else if (currentSlot == assignedSlot) {
        StateMachine_setNextState(&stateMachine, tdmaTransmitState);
    } else {
        StateMachine_setNextState(&stateMachine, tdmaReceiveState);
    }

    Semaphore_post(slotSemaphore);
    StateMachine_postEvents(&stateMachine, Event_SlotChanged);
}

void receiverTaskFunction(UArg arg0, UArg arg1)
{
    while (1) {
        Semaphore_pend(slotSemaphore, BIOS_WAIT_FOREVER);

        stateMachine.pendingEvents = 0;
        stateMachine.currentState = stateMachine.nextState;
        stateMachine.transitionPending = false;

        stateMachine.currentState();
    }
}

void tdmaSyncStateFunction()
{
    PIN_setOutputValue(ledPinHandle, Board_LED0, 0);
    PIN_setOutputValue(ledPinHandle, Board_LED1, 1);

    Clock_stop(clkHandle);

    rfCmdHandle = RF_postCmd(rfHandle, (RF_Op*)&RF_cmdPropRx, RF_PriorityNormal, &rfRxCallback, IRQ_RX_ENTRY_DONE);

    while (1) {
        StateMachine_EventMask events = StateMachine_pendEvents(&stateMachine, STATE_MACHINE_EVENTS_ALL, BIOS_WAIT_FOREVER);

        if (events & Event_PacketReceived) {
            // process the received packet
            union TDMAPacket *pkt = (union TDMAPacket *)rxPacket;
            if(pkt->header.packetType == TDMA_PACKET_TYPE_INFO) {
                timeOffset = pkt->infoPacket.timestamp - rxTime;
                int32_t remaining = pkt->infoPacket.nextTime - rxTime - timeOffset;
                Clock_setTimeout(clkHandle, RF_convertRatTicksToUs(remaining)/Clock_tickPeriod);
                Clock_setPeriod(clkHandle, (pkt->infoPacket.slotLen*1000)/Clock_tickPeriod);
                break;
            }
        }
    }

    currentSlot = 0;

    Clock_start(clkHandle);

    RF_cancelCmd(rfHandle, rfCmdHandle, 0); // End the permanent RxCmd
}

void tdmaRequestStateFunction()
{
    PIN_setOutputValue(ledPinHandle, Board_LED0, 1);
    PIN_setOutputValue(ledPinHandle, Board_LED1, 1);

    uint32_t nonce = 0; // TODO

    union TDMAPacket message;
    message.header.destinationAddress = TDMA_ADDRESS_BROADCAST;
    message.header.sourceAddress = TDMA_ADDRESS_COORDINATOR;
    message.header.packetType = TDMA_PACKET_TYPE_REQUEST;
    message.requestPacket.nonce = nonce;
    memcpy(txPacket, &message, sizeof(struct TDMARequest));

    RF_cmdPropTx.startTrigger.triggerType = TRIG_NOW;
    RF_cmdPropTx.pktLen = sizeof(struct TDMARequest);

    RF_cmdPropRx.startTrigger.triggerType = TRIG_NOW;
    RF_cmdPropRx.endTrigger.triggerType = TRIG_NEVER;

    RF_EventMask result = RF_runCmd(rfHandle, (RF_Op *)&RF_cmdPropTx, RF_PriorityNormal, NULL, 0);
    if (!(result & RF_EventLastCmdDone)) {
        System_abort("Error transmitting beacon\n");
    }

    rfCmdHandle = RF_postCmd(rfHandle, (RF_Op*)&RF_cmdPropRx, RF_PriorityNormal, &rfRxCallback, RF_EventRxEntryDone);

    while (1) {
        StateMachine_EventMask events = StateMachine_pendEvents(&stateMachine, Event_SlotChanged | Event_PacketReceived, BIOS_WAIT_FOREVER);

        // If a slot change has occurred, advance the state
        if (events & Event_SlotChanged) {
            break;
        }

        if (events & Event_PacketReceived) {
            union TDMAPacket *pkt = (union TDMAPacket *)rxPacket;
            if(pkt->header.packetType == TDMA_PACKET_TYPE_ASSIGN) {
                if (pkt->assignPacket.nonce == nonce) {
                    assignedSlot = pkt->assignPacket.slot;
                }
                break;
            }
        }
    }

    RF_cancelCmd(rfHandle, rfCmdHandle, 1); // End the permanent RxCmd
}

void tdmaTransmitStateFunction()
{
    PIN_setOutputValue(ledPinHandle, Board_LED0, 1);
    PIN_setOutputValue(ledPinHandle, Board_LED1, 0);
}

void tdmaReceiveStateFunction()
{
    PIN_setOutputValue(ledPinHandle, Board_LED0, 0);
    PIN_setOutputValue(ledPinHandle, Board_LED1, 0);

    /*
    RF_cmdPropRx.startTrigger.triggerType = TRIG_NOW;
    RF_cmdPropRx.endTrigger.triggerType = TRIG_REL_START;
    RF_cmdPropRx.endTime = RX_START_TO_SETTLE_TICKS + RX_TIMEOUT_TICKS + RX_TIMEOUT_MARGIN + RX_START_MARGIN;

    while (1) {
        //RF_CmdHandle cmd = RF_postCmd(rfHandle, (RF_Op*)&RF_cmdPropRx, RF_PriorityNormal, &tdmaAssignmentStateCallback, RF_EventRxEntryDone);

        StateMachine_EventMask events = StateMachine_pendEvents(&stateMachine, Event_SlotChanged | Event_PacketReceived, BIOS_WAIT_FOREVER);

        // If a slot change has occurred, check if state change is needed
        if (events & Event_SlotChanged) {
            if (currentSlot == TDMA_SLOT_BEACON) {
                StateMachine_setNextState(&stateMachine, tdmaSyncState);
                break;
            } else if (assignedSlot != TDMA_SLOT_REQUEST && currentSlot == assignedSlot) {
                StateMachine_setNextState(&stateMachine, tdmaTransmitState);
                break;
            } else {
                StateMachine_setNextState(&stateMachine, tdmaReceiveState);
                break;
            }
        }

        if (events & Event_PacketReceived) {
            // Check if it was addressed to us
        }
    }
    */
}

void rfRxCallback(RF_Handle h, RF_CmdHandle ch, RF_EventMask e) {
    if(e & RF_EventRxEntryDone) {
        /* Get current unhandled data entry */
        rfc_dataEntryGeneral_t *currentDataEntry = RFQueue_getDataEntry();

        uint8_t len = currentDataEntry->data;
        memcpy(rxPacket, &currentDataEntry->data + 1, len);
        memcpy(&rxTime, &currentDataEntry->data + 1 + len, sizeof(uint32_t));

        struct TDMAHeader *header = (struct TDMAHeader *)rxPacket;

        switch (header->packetType) {
        case TDMA_PACKET_TYPE_INFO:
            StateMachine_postEvents(&stateMachine, Event_PacketReceived);
            break;
        case TDMA_PACKET_TYPE_REQUEST:
            StateMachine_postEvents(&stateMachine, Event_PacketReceived);
            break;
        case TDMA_PACKET_TYPE_ASSIGN:
            StateMachine_postEvents(&stateMachine, Event_PacketReceived);
            break;
        case TDMA_PACKET_TYPE_DATA:
            StateMachine_postEvents(&stateMachine, Event_PacketReceived);
            break;
        default:
            // Invalid packet
            StateMachine_postEvents(&stateMachine, Event_InvalidPacketReceived);
            break;
        }

        RFQueue_nextEntry();
    }
}
